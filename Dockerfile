FROM python:3.8
WORKDIR /kafka_faust_app/
COPY . .
RUN pip3 install -r requirements.txt
ENTRYPOINT ["/bin/sh","/kafka_faust_app/entrypoint.sh"]
