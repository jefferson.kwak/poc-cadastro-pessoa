#!/bin/bash

entrypoint(){
    cd /kafka_faust_app/
    sleep 20
    watchmedo auto-restart --recursive -d . -p '*.py' -i '.*pyc' -- \
        faust -A people worker -l info
}

entrypoint
