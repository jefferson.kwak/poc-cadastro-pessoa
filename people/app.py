import faust


app = faust.App(
    'people',
    broker='kafka://kafka',
    autodiscover=True
)

def main():
    app.main()
