
from people import app
from people.users.models import User
from people.users.views import People
from people.database import session
from people.database.models.user_config import UserConfig


user_topic = app.topic('users', key_type=str, value_type=User)
document_topic = app.topic('validate_document', key_type=str)
user_response_topic = app.topic('user_response', key_type=str)
user_language_topic = app.topic('language', key_type=str)

@app.agent(user_topic)
async def user_topic_handler(stream):
    async for event in stream:
        print(f"User Received: {event}")
        config = session.query(UserConfig).filter(
            UserConfig.config_name==event.config).one_or_none()

        if config.value:
            People(
                payload=event.payload, 
                query=event.query,
                method=event.action
            ).action()

@app.agent(document_topic)
async def document_topic_handler(stream):
    async for event in stream:
        print(f"Received validator document: {event}")

@app.agent(user_response_topic)
async def user_response_handler(stream):
    async for event in stream:
        print(f"User Response Received: {event}")

@app.agent(user_language_topic)
async def user_language_handler(stream):
    async for event in stream:
        print(f"User language Received: {event}")
