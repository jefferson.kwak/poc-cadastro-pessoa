from people.database import session
from people.database.models.user import User
from people.users.producer import Producer

from uuid import uuid4
from sqlalchemy import or_


class People:
    def __init__(self, payload, query, method):
        self._payload = payload
        self._query = query
        self._method = method
        self._method_map = {
            "atualizar": self._update,
            "cadastrar": self._register,
            "buscar": self._get
        }
        self._producer = Producer("kafka")
    
    def action(self):
        try:
            return self._method_map[self._method]()
        except KeyError:
            raise Exception("This action do not exist.")
    
    def _update(self):
        try:
            cpf = self._payload.pop("cpf", None)
            id = self._payload.pop("id", None)
            user_id = session.query(User).filter(
                or_(
                    User.cpf==cpf, 
                    User.id==id
                )
            ).update(self._payload)
            session.commit()
            user = session.query(User).filter(
                User.id==user_id
            ).one_or_none()
            self._producer.send(
                "user_response", {
                    "name": user.name,
                    "cpf": user.cpf,
                    "birth_date": user.birth_date,
                    "language": user.language,
                    "nationality": user.nationality,
                    "id_interno": str(user.id_interno),
                    "id": user.id
                }
            )
        except KeyError:
            raise Exception("payload must have cpf to update an user.")

    def _register(self):
        self._payload["id_interno"] = self._generate_unexisting_uuid()
        self._producer.send(
            topic="validate_document", 
            message={
                "cpf": self._payload["cpf"], 
                "id": self._payload["id_interno"]
            }
        )
        if self._payload["nationality"]:
            self._producer.send(
                topic="language",
                message={
                    "nationality": self._payload["nationality"],
                    "id": self._payload["id_interno"]
                }
            )

        session.add(User(**self._payload))
        session.commit()
        user = session.query(User).filter(
            User.cpf==self._payload["cpf"]
        ).one_or_none()
        self._producer.send(
            "user_response", {
                "name": user.name,
                "cpf": user.cpf,
                "birth_date": user.birth_date,
                "language": user.language,
                "nationality": user.nationality,
                "id_interno": str(user.id_interno),
                "id": user.id
            }
        )

    def _get(self):
        try:
            cpf = self._query["cpf"]
            id = self._query["id"]
            user = session.query(User).filter(
                User.cpf==cpf,
                User.id==id
            ).one_or_none()
            if not user:
                self._producer.send(
                    "user_response",
                    "No user found."
                )

            self._producer.send(
                "user_response", {
                    "name": user.name,
                    "cpf": user.cpf,
                    "birth_date": user.birth_date,
                    "language": user.language,
                    "nationality": user.nationality,
                    "id_interno": str(user.id_interno),
                    "id": user.id
                }
            )
        except KeyError as e:
            raise Exception(f"Missing {e} field in message.")

    def _generate_unexisting_uuid(self):
        id_interno = uuid4()
        user = session.query(User).filter(
            User.id_interno==id_interno).one_or_none()
        return str(id_interno if not user else self._generate_unexisting_uuid())
