import faust


class User(faust.Record, validation=True):
    action: str
    config: str
    payload: dict = None
    query: dict = None
