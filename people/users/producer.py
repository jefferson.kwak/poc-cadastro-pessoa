from kafka import KafkaProducer
import json

class Producer:
    def __init__(self, hostnport):
        self._producer = KafkaProducer(
            bootstrap_servers=[hostnport],
            value_serializer=lambda m: json.dumps(m).encode('ascii'))

    def send(self, topic, message):
        self._producer.send(topic, message)
        self._producer.flush()


if __name__ == "__main__":
    data_get = {
        "action": "buscar", 
        "config": "grupodoido", 
        "query": {
            "cpf": "99941918023",
            "id": 1
        }
    }
    data_update = {
        "action": "atualizar", 
        "config": "grupodoido", 
        "payload": {
            "id": 1,
            "nationality": "brasileiro"
        }
    }
    data_register = {
        "action": "cadastrar",
        "config": "any",
        "payload": {
            "id_interno":"94a98262-0f13-4e64-aeca-b2ce4312068a",
            "cpf": "99941918023",
            "name": "jose"
        }
    }
    p = Producer("localhost:29092")
    p.send("users", data_update)
