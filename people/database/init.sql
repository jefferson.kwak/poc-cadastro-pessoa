CREATE TABLE IF NOT EXISTS peoples(
    id SERIAL,
    id_interno uuid,
    cpf varchar(20),
    name varchar(50),
    birth_date date,
    nationality varchar(50),
    language varchar(50),
    created timestamptz,
    updated timestamptz
);

CREATE TABLE IF NOT EXISTS peoples_config(
    id SERIAL,
    "group" varchar(50),
    config_name varchar(100),
    value Boolean
);
