from sqlalchemy.dialects.postgresql import UUID
from people.database import Model
from people.database import Base
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import String
from sqlalchemy import Boolean


class User(Model):
    __tablename__ = "peoples"
    
    name = Column(String)
    id_interno = Column(UUID(as_uuid=True))
    cpf = Column(String)
    birth_date = Column(Date)
    nationality = Column(String)
    language = Column(String)
