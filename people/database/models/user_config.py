from people.database import Base
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Boolean


class UserConfig(Base):
    __tablename__ = "peoples_config"

    id = Column(Integer, primary_key=True, index=True)
    group = Column(String)
    config_name = Column(String)
    value = Column(Boolean)
