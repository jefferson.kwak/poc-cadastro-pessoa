from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import Column, DateTime, Integer, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func


session = scoped_session(
    sessionmaker(
        autocommit=False,
        autoflush=False,
        expire_on_commit=False,
        bind=create_engine(
            "postgresql://teste:teste@database:5432/people_database",
            pool_size=1,
            max_overflow=25,
            pool_recycle=30 * 60,
        ),
    )
)

Base = declarative_base()
Base.query = session.query_property()

class Model(Base):
    __abstract__ = True

    id = Column(Integer, primary_key=True, index=True)
    created = Column(DateTime(timezone=True), server_default=func.now())
    updated = Column(
        DateTime(timezone=True),
        server_onupdate=func.utc_timestamp(),
        server_default=func.now(),
    )
